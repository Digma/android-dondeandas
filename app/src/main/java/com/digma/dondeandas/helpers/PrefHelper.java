package com.digma.dondeandas.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by janidham on 28/01/16.
 */
public class PrefHelper {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared pref file name
    private static final String PREF_NAME = "DondeAndas";

    // All Shared Preferences Keys of User
    public static final String DONE_TUTORIAL = "DONE_TUTORIAL";

    // Constructor
    public PrefHelper(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setDoneTutorial(boolean done) {
        editor.putBoolean(DONE_TUTORIAL, done);
        editor.commit();
    }

    public boolean getDoneTutorial() { return pref.getBoolean(DONE_TUTORIAL, false); }
}
