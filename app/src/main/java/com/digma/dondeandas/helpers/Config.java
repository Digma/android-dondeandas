package com.digma.dondeandas.helpers;

/**
 * Created by janidham on 28/01/16.
 */
public class Config {

    public static final String BASE_URL = "http://www.dondeandas.com.mx/sistema/";
    public static final String IMG_BASE_URL = "http://www.dondeandas.com.mx/";

    public static final String CHAT_SERVER_URL = "http://192.168.1.65:3000";
}
