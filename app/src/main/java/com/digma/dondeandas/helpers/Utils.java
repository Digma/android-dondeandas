package com.digma.dondeandas.helpers;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by janidham on 03/02/16.
 */
public class Utils {

    private Context context;

    public Utils(Context context) {
        this.context = context;
    }

    public void renderToask(String message, int time) {
        Toast.makeText(context, message, time).show();
    }
}
