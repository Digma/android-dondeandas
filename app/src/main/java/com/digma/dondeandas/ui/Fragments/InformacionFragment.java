package com.digma.dondeandas.ui.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digma.dondeandas.R;
import com.digma.dondeandas.ui.EmpresaMainActivity;


public class InformacionFragment extends Fragment {
    TextView nombreEmpresa;
    TextView descLarga;
    TextView descCorta;
    TextView direccion;
    TextView correo;
    TextView telefono;
    TextView celular;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_informacion, container, false);

        ((EmpresaMainActivity) getActivity()).getSupportActionBar().setTitle("Descripción");
        nombreEmpresa = (TextView) view.findViewById(R.id.textView6);
        descLarga = (TextView) view.findViewById(R.id.textView);
        descCorta = (TextView) view.findViewById(R.id.textView7);
        direccion = (TextView) view.findViewById(R.id.textView8);
        correo = (TextView) view.findViewById(R.id.textView9);
        telefono = (TextView) view.findViewById(R.id.textView10);
        celular = (TextView) view.findViewById(R.id.textView11);
        nombreEmpresa.setText(EmpresaMainActivity.customer.getTitle());
        descLarga.setText(EmpresaMainActivity.customer.getLongDescription());
        descCorta.setText(EmpresaMainActivity.customer.getShortDescription());
        direccion.setText(EmpresaMainActivity.customer.getDireccion());
        correo.setText("Correo: " + EmpresaMainActivity.customer.getEmail());
        telefono.setText("Teléfono: " + EmpresaMainActivity.customer.getTelefono());
        celular.setText("Celular: " + EmpresaMainActivity.customer.getCelular());


        return view;
    }

}
