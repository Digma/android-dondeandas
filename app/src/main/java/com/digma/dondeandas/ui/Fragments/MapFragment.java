package com.digma.dondeandas.ui.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import android.widget.Toast;

import com.digma.dondeandas.R;
import com.digma.dondeandas.domain.Place;
import com.digma.dondeandas.helpers.ChatApplication;
import com.digma.dondeandas.helpers.Config;
import com.digma.dondeandas.helpers.MapHelpers;
import com.digma.dondeandas.helpers.Utils;
import com.digma.dondeandas.io.PlaceApiInterface;
import com.digma.dondeandas.ui.EmpresaMainActivity;
import com.google.android.gms.location.internal.LocationRequestUpdateData;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by janidham on 26/01/16.
 */
public class MapFragment extends Fragment
        implements OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowLongClickListener,
        GoogleMap.OnMyLocationButtonClickListener {

    public static final String TAG = "MapFragment";


    private GoogleMap mMap;

    private MapHelpers mapHelper;

    List<Place> places;
    public static LatLng latLngToGo;

    public List<Place> tour;
    public Place tmpPlace;
    public int indexEndTour;

    public static Context context;

    private Utils utils;

    HashMap<Marker, Place> haspMap = new HashMap<Marker, Place>();

    private HashMap<String, Marker> haspFriends;


    private Socket mSocket;
    //Inicia la instacia del  fragment map con todos los lugares dados de alta en la base de datos.
    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    //Inicia la instacia del  fragment map con un sólo lugar especifico para trazar la ruta.
    public static MapFragment newInstance(LatLng latLng) {
        MapFragment fragment = new MapFragment();
        latLngToGo = latLng;

        return fragment;
    }

    public MapFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        //ChatApplication app = (ChatApplication) getActivity().getApplication();
        try {
            mSocket = IO.socket(Config.CHAT_SERVER_URL);

            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);

            mSocket.on("friend joined", onUserJoined);
            mSocket.on("friend disconneted", onFrinedDisconnected);

            mSocket.connect();

            haspFriends = new HashMap<String, Marker>();

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        //mSocket = app.getSocket();

    }

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String name;
                    double latitud, longitud;
                    try {

                        name = data.getString("name");
                        latitud = data.getDouble("latitud");
                        longitud = data.getDouble("longitud");

                        renderFrined(name, latitud, longitud);

                        Log.d(TAG, latitud + " " + longitud);
                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener onFrinedDisconnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String name;

                    try {

                        name = data.getString("name");

                        Marker m = haspFriends.get(name);
                        m.remove();

                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };

    private void renderFrined(String name, double latitud, double longitud) {
        Marker m = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitud, longitud))
                .title(name)
                .snippet("Tu mejor amigo")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        haspFriends.put(name, m);

    }



    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error al conectar", Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        context = getContext();
        utils = new Utils(getContext());

        tour = new ArrayList<>();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                 .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        return view;
    }

    public void getPlacesToMap() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PlaceApiInterface service = retrofit.create(PlaceApiInterface.class);
        Call<List<Place>> getPlaces = service.getAllPlaces();

        getPlaces.enqueue(new Callback<List<Place>>() {
            @Override
            public void onResponse(Call<List<Place>> call, Response<List<Place>> response) {
                places = response.body();

                if (places == null) return;

                for (Place place : places) {
                    Marker m = mMap.addMarker(new MarkerOptions()
                            .position(place.getPosition())
                            .title(place.getTitle())
                            .snippet(place.getShortDescription()));

                    haspMap.put(m, place);
                }

                mMap.animateCamera(CameraUpdateFactory
                        .newLatLng(places.get(0).getPosition()));
            }

            @Override
            public void onFailure(Call<List<Place>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mapHelper = new MapHelpers(mMap, getContext());

        if (mapHelper.isPermissionGranted()) {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
        }

        if (latLngToGo == null) {
            getPlacesToMap();
            mMap.setOnInfoWindowClickListener(this);
            mMap.setOnInfoWindowLongClickListener(this);

            Location l = mapHelper.getCurrentLocation();

            if (l != null)
                mSocket.emit("joined", l.getLatitude() + "", l.getLongitude() + "");

            return;
        }

        Marker m = mMap.addMarker(new MarkerOptions()
                .position(latLngToGo)
                .title(EmpresaMainActivity.customer.getTitle())
                .snippet(EmpresaMainActivity.customer.getShortDescription()));

        //haspMap.put(m, EmpresaMainActivity.customer);

        mMap.moveCamera(CameraUpdateFactory
                .newLatLng(latLngToGo));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (latLngToGo != null)
            latLngToGo = null;
        Log.d(TAG, "destroyview");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("friend joined", onUserJoined);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        if (id == R.id.action_start_tour) {
            if (tour == null || tour.size() == 0) {
                //final Utils utils = new Utils(getContext());
                utils.renderToask("Debe agregar lugar al tour primero.", Toast.LENGTH_LONG);

                return true;
            }

            if (mapHelper.getCurrentLocation() == null) return true;

            renderDialogStartTour();

            Log.d(TAG, "Iniciando tour..");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        renderDialog(marker.getPosition());
    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        int placeID = haspMap.get(marker) != null ? haspMap.get(marker).getId() : -1;

        Intent intent = new Intent(getContext(), EmpresaMainActivity.class);
        intent.putExtra(EmpresaMainActivity.PLACE_ID, placeID);

        startActivity(intent);
    }

    public void renderDialogStartTour() {
        final int tourSize = tour.size();

        if (tourSize == 1) {
            //startTour(0);
            mapHelper.startTour(tour, 0);
            return;
        }

        final CharSequence[] places = new CharSequence[tourSize];
        int i = 0;

        for (Place place : tour) {
            places[i] = place.getTitle();
            ++i;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.tour_dialog_title)
                .setSingleChoiceItems(places, -1,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                indexEndTour = item;
                            }
                        }
                ).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //startTour(indexEndTour);
                        mapHelper.startTour(tour, indexEndTour);

                        dialog.dismiss();
                    }
                }
                ).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );

        builder.create();
        builder.show();

    }

    public void renderDialog(LatLng position) {

        for (Place place : places) {
            if (place.getLatitud() == position.latitude && place.getLongitud() == position.longitude) {
                tmpPlace = place;
                break;
            }
        }

        final Utils utils = new Utils(getContext());

        final Dialog dialog = new Dialog(getContext());

        dialog.setContentView(R.layout.dialog_place_options);
        dialog.setTitle(R.string.dialog_title);

        final Button btnViewPlace = (Button) dialog.findViewById(R.id.dialog_btn_view);
        final Button btnCallPlace = (Button) dialog.findViewById(R.id.dialog_btn_call);
        final Button btnQualifyPlace = (Button) dialog.findViewById(R.id.dialog_btn_qualify);
        final Button btnCancel = (Button) dialog.findViewById(R.id.dialog_btn_cancel);
        final Button btnOk = (Button) dialog.findViewById(R.id.dialog_btn_ok);
        final CheckBox chkAddTour = (CheckBox) dialog.findViewById(R.id.dialog_chk_add_tour);

        btnViewPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.renderToask("Click en ver..", Toast.LENGTH_SHORT);
            }
        });

        btnCallPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.renderToask("Click en llamar..", Toast.LENGTH_SHORT);
            }
        });

        btnQualifyPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.renderToask("Click en calificar..", Toast.LENGTH_SHORT);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkAddTour.isChecked())
                    tour.add(tmpPlace);

                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public boolean onMyLocationButtonClick() {
        Log.d(TAG, "My location button click");
        Location l = mapHelper.getCurrentLocation();

        if (l != null)
            mSocket.emit("joined", l.getLatitude(), l.getLongitude());

        //mapHelper.googleAPILocation();
        return false;
    }

    public void createPlaces() {
        places = new ArrayList<>();

        //places.add(new Place(4, "Mi casa", "yo", "", 19.837898, -90.547262, 0));
        places.add(new Place(5, "Puerta de Tierra", "Baluarte", "", 19.841328, -90.535943, 1200));
        places.add(new Place(1, "Puerta de Mar", "Baluarte.", "", 19.845848, -90.538435, 1000));
        places.add(new Place(2, "Baluarte Soledad", "Baluarte.", "", 19.846213, -90.537779, 1100));
        places.add(new Place(3, "Instituto Campechano", "Escuela", "", 19.843840, -90.539886, 1000));
        places.add(new Place(6, "Catedral", "Iglesia", "", 19.845911, -90.536495, 1500));

    }

}
