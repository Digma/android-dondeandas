package com.digma.dondeandas.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.digma.dondeandas.R;
import com.digma.dondeandas.domain.Customer;
import com.digma.dondeandas.helpers.Config;
import com.digma.dondeandas.io.PlaceApiInterface;
import com.digma.dondeandas.ui.Fragments.CalificarFragment;
import com.digma.dondeandas.ui.Fragments.InformacionFragment;
import com.digma.dondeandas.ui.Fragments.MapFragment;
import com.digma.dondeandas.ui.Fragments.NosotrosFragment;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class EmpresaMainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ActionBar actionBar;
    Intent intent;
    Intent i;
    Boolean validateData = false;

    //El objeto que contiene todos los datos que te manda pomuch
    public static String urlRed;
    public static Customer customer;
    public static final String PLACE_ID = "PLACE_ID";
    int placeID;

    MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresamain);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nosotros");
        getData();
        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        setupNavigationDrawerContent(navigationView);
        intent = getIntent();
        placeID = intent.getIntExtra(PLACE_ID, -1);
        if(!validateData){
            getData();
        }
        if (savedInstanceState == null) {
            Log.d("onCreate", "nulo");
        } else
            Log.d("onCreate", "no nulo");

        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);
        }
    }

    //metodo con el cual podras poner toda la info en donde necesites, ejemplo como la validacion del face y otra redes
    //sociales.
    public void onCustomerCreated() {
        //solo para verificar que no esté nulo y no vaya a crashear
        if (customer == null) return;

        //Es un ejemplo que si el face esta vacío se desability la opcion de menu;
        if (customer.getFacebook().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(0).setEnabled(false);
        if (customer.getTwitter().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(1).setEnabled(false);
        if (customer.getInstagram().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(2).setEnabled(false);
        if (customer.getYoutube().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(3).setEnabled(false);
        if (customer.getGoogleplus().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(4).setEnabled(false);
        if (customer.getUrl().equals(""))
            navigationView.getMenu().getItem(6).getSubMenu().getItem(5).setEnabled(false);


    }

    public void getData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PlaceApiInterface service = retrofit.create(PlaceApiInterface.class);
        Call<Customer> getInfoCustomer = service.getCustomerbyId(placeID);
        //Call<Customer> getInfoCustomer = service.getCustomerbyId(1);
        getInfoCustomer.enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                customer = response.body();
                onCustomerCreated();


                ImageView img_menu_header = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.img_menu_header);
                String imgURL = String.format(Config.IMG_BASE_URL + customer.getImgMenu());
                Picasso.with(getApplicationContext())
                        .load(imgURL)
                        .into(img_menu_header);
                validateData = true;
                setFragment(0);
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.item_nosotros:
                                //menuItem.setChecked(true);
                                setFragment(0);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            // Con el proyecto de Janid
                            case R.id.item_ubicanos:
                                //menuItem.setChecked(true);
                                setFragment(3);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_descripcion:
                                //menuItem.setChecked(true);
                                setFragment(1);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_calificar:
                                //menuItem.setChecked(true);
                                setFragment(2);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_llamar:
                                //menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                try {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                                            return true;

                                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + customer.getCelular())));

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return true;
                            case R.id.item_galeria:
                                // menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                intent = new Intent(EmpresaMainActivity.this, GaleriaActivity.class);
                                startActivity(intent);
                                return true;
                            case R.id.item_face:
                                //   redes.setData(Uri.parse(customer.getFacebook()));
                                //  startActivity(redes);
                                //   menuItem.setChecked(true);
                                urlRed = customer.getFacebook();
                                startSocialNetwork("com.facebook.katana", "fb://profile/" + customer.getUserfacebook(), urlRed);
                                //setFragment(4);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_twitter:
                                //menuItem.setChecked(true);
                                urlRed = customer.getTwitter();
                                startSocialNetwork("com.twitter.android", "twitter://user?screen_name=" + customer.getUsertwitter(), urlRed);
                                //setFragment(4);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_instagram:
                                // menuItem.setChecked(true);
                                urlRed = customer.getInstagram();
                                //startSocialNetwork("", "", customer.getInstagram());
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(urlRed));
                                startActivity(i);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_google:
                                // menuItem.setChecked(true);
                                urlRed = customer.getGoogleplus();
                                //startSocialNetwork("", "", customer.getGoogleplus());
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(urlRed));
                                startActivity(i);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_youtube:
                                //  menuItem.setChecked(true);
                                urlRed = customer.getYoutube();
                                //startSocialNetwork("", "", customer.getYoutube());
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(urlRed));
                                startActivity(i);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_pagina:
                                //  menuItem.setChecked(true);
                                urlRed = customer.getUrl();
                                //startSocialNetwork("", "", customer.getUrl());
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(urlRed));
                                startActivity(i);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                        }
                        return true;
                    }
                });
    }

    public void startSocialNetwork(String app, String profile, String url) {
        Intent intent = createIntenetBySN(app, profile, url);

        if (intent == null) return;

        startActivity(intent);

    }

    public Intent createIntenetBySN(String app, String profile, String url) {
        if (app == null || url == null)
            return null;
        else if (app.isEmpty() && profile.isEmpty() && !url.isEmpty())
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url));

        try {
            getPackageManager()
                    .getPackageInfo(app, 0); //Checks if app is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse(profile)); //Trys to make intent with app's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url)); //catches and opens a url to the desired page
        }

    }

    public void setFragment(int position) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (position) {
            case 0:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                NosotrosFragment nosotrosFragment = new NosotrosFragment();
                fragmentTransaction.replace(R.id.fragment, nosotrosFragment);
                fragmentTransaction.commit();
                break;
            case 1:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                InformacionFragment informacionFragment = new InformacionFragment();
                fragmentTransaction.replace(R.id.fragment, informacionFragment);
                fragmentTransaction.commit();
                break;
            case 2:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                CalificarFragment calificarFragment = new CalificarFragment();
                fragmentTransaction.replace(R.id.fragment, calificarFragment);
                fragmentTransaction.commit();
                break;
            case 3:
                LatLng latLng = new LatLng(customer.getLatitud(), customer.getLongitud());
                Fragment mapFragment = MapFragment.newInstance(latLng);
                fragmentManager = getSupportFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment, mapFragment)
                        .commit();
                /*
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MapFragment mapFragment = new MapFragment();
                fragmentTransaction.replace(R.id.fragment, mapFragment);
                fragmentTransaction.commit();
                */
                break;
        }
    }
}

