package com.digma.dondeandas.ui.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digma.dondeandas.R;
import com.digma.dondeandas.helpers.Config;
import com.digma.dondeandas.io.PlaceApiInterface;
import com.digma.dondeandas.io.model.ServerResponse;
import com.digma.dondeandas.ui.EmpresaMainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class CalificarFragment extends Fragment {
    private static RatingBar ratingBar;
    private static TextView nombreEmpresa;
    private static TextView btnSubmit;

    TextView txtComment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_calificar, container, false);



        ((EmpresaMainActivity) getActivity()).getSupportActionBar().setTitle("Calificar");


        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        nombreEmpresa = (TextView) view.findViewById(R.id.textView2);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        txtComment = (TextView) view.findViewById(R.id.editText);
        nombreEmpresa.setText(EmpresaMainActivity.customer.getTitle());
        addListenerOnRatingBar();
        addListenerOnButton();
        return view;
    }
    public void addListenerOnRatingBar() {



        //if rating value is changed,
        //display the current rating value in the result (textview) automatically

     /*   ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                resultado.setText(String.valueOf(rating));

            }
        });*/
    }

    public void addListenerOnButton() {



        //if click on me, then display the current rating value.
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int id = EmpresaMainActivity.customer.getId();
                float score = ratingBar.getRating();
                String comment = txtComment.getText().toString();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Config.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                PlaceApiInterface service = retrofit.create(PlaceApiInterface.class);
                Call<ServerResponse> rate = service.rateCustomer(id, score, comment);
                rate.enqueue(new Callback<ServerResponse>() {
                    @Override
                    public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                        Log.d("Reponse", response.code() + "");
                    }

                    @Override
                    public void onFailure(Call<ServerResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
                //Call<List<Place>> getPlaces = service.getAllPlaces();

                Toast.makeText(getActivity(),
                        String.valueOf(ratingBar.getRating()),
                        Toast.LENGTH_SHORT).show();

            }

        });

    }



}
