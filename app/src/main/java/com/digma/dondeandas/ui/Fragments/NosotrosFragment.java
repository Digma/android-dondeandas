package com.digma.dondeandas.ui.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digma.dondeandas.R;
import com.digma.dondeandas.domain.Customer;
import com.digma.dondeandas.helpers.Config;
import com.digma.dondeandas.ui.EmpresaMainActivity;
import com.squareup.picasso.Picasso;


public class NosotrosFragment extends Fragment {
    ImageView portadaImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_nosotros, container, false);

        ((EmpresaMainActivity) getActivity()).getSupportActionBar().setTitle("Nosotros");

        portadaImageView = (ImageView) view.findViewById(R.id.imageView);
        Picasso.with(getContext())
                .load(Config.IMG_BASE_URL + EmpresaMainActivity.customer.getImgPortada())
                .into(portadaImageView);

        return view;
    }

}
