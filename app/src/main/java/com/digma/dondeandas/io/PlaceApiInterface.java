package com.digma.dondeandas.io;

import com.digma.dondeandas.domain.Customer;
import com.digma.dondeandas.domain.Place;
import com.digma.dondeandas.io.model.ServerResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by janidham on 10/02/16.
 */
public interface PlaceApiInterface {


    @GET("placements")
    Call<List<Place>> getAllPlaces();

    @GET("customers/{id}")
    Call<Customer> getCustomerbyId(@Path("id") int id);

    @FormUrlEncoded
    @POST("rateclient")
    Call<ServerResponse> rateCustomer(@Field("clientid") int customerID, @Field("score") float score,
                                   @Field("comment") String comment);

    @GET("users/{user}/repos")
    Call<List<Place>> listRepos(@Path("user") String user);

    @GET("shows")
    Call<List<Show>> getAllShows();

    @GET("shows/{id}")
    Call<Show> getShowbyId(@Path("id") int id);
}