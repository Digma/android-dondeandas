package com.digma.dondeandas.io;

import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 10/02/16.
 */
public class Show {

    @SerializedName("id")
    int mId;

    @SerializedName("name")
    String mName;

    public Show(int id, String name) {
        this.mId = id;
        this.mName = name;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
