package com.digma.dondeandas.io.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 10/02/16.
 */
public class ServerResponse {

    @SerializedName("message")
    String message;

}
