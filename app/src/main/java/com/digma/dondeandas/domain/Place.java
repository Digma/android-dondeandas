package com.digma.dondeandas.domain;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 28/01/16.
 */
public class Place {

    /*
    private int id, duration;
    private String title, shortDescription, urlImage;
    private double latitud, longitud;
    */
    @SerializedName("id")
    int id;
    @SerializedName("duration")
    int duration;
    @SerializedName("nombre_comercial")
    String title;
    @SerializedName("descripcion_corta")
    String shortDescription;
    @SerializedName("imagen_pin")
    String urlImage;
    @SerializedName("latitud")
    double latitud;
    @SerializedName("longitud")
    double longitud;

    private LatLng position;

    public Place(int id, String title, String shortDescription, String urlImage, double latitud, double longitud, int duration) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.urlImage = urlImage;
        this.latitud = latitud;
        this.longitud = longitud;
        this.duration = duration;

        this.setPosition();

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) { this.shortDescription = shortDescription; }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getDuration() { return duration; }

    public void setDuration(int duration) { this.duration = duration; }

    public void setPosition() {
        this.position = new LatLng(this.latitud, this.longitud);
    }

    public LatLng getPosition() {
        if (this.position == null) this.position = new LatLng(this.latitud, this.longitud);

        return this.position;
    }
}
