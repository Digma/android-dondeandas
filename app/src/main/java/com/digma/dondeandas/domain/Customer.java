package com.digma.dondeandas.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alejandroaranda on 24/02/16.
 */
public class Customer {
    @SerializedName("id")
    int id;
    @SerializedName("descripcion_larga")
    String longDescription;
    @SerializedName("nombre_comercial")
    String title;
    @SerializedName("descripcion_corta")
    String shortDescription;
    @SerializedName("imagen_pin")
    String urlImage;
    @SerializedName("latitud")
    double latitud;
    @SerializedName("longitud")
    double longitud;
    @SerializedName("telefono")
    String telefono;
    @SerializedName("celular")
    String celular;
    @SerializedName("url")
    String url;
    @SerializedName("email")
    String email;
    @SerializedName("facebook")
    String facebook;
    @SerializedName("twitter")
    String twitter;
    @SerializedName("instagram")
    String instagram;
    @SerializedName("googleplus")
    String googleplus;
    @SerializedName("youtube")
    String youtube;
    @SerializedName("imagen_portada")
    String imgPortada;
    @SerializedName("imagen_menu")
    String imgMenu;
    @SerializedName("img1")
    String img1;
    @SerializedName("img2")
    String img2;
    @SerializedName("img3")
    String img3;
    @SerializedName("img4")
    String img4;
    @SerializedName("img5")
    String img5;
    @SerializedName("direccion")
    String direccion;
    @SerializedName("userfacebook")
    String userfacebook;
    @SerializedName("usertwitter")
    String usertwitter;
    public String[] images = new String[5];

    public Customer(int id, String longDescription, String title, String youtube, String instagram, String email, String shortDescription, double latitud, String urlImage, double longitud, String telefono, String celular, String url, String facebook, String twitter, String googleplus, String imgPortada, String imgMenu, String img1, String img2, String img4, String img3, String img5, String direccion, String userfacebook, String usertwitter) {
        this.id = id;
        this.longDescription = longDescription;
        this.title = title;
        this.youtube = youtube;
        this.instagram = instagram;
        this.email = email;
        this.shortDescription = shortDescription;
        this.latitud = latitud;
        this.urlImage = urlImage;
        this.longitud = longitud;
        this.telefono = telefono;
        this.celular = celular;
        this.url = url;
        this.facebook = facebook;
        this.twitter = twitter;
        this.googleplus = googleplus;
        this.imgPortada = imgPortada;
        this.imgMenu = imgMenu;
        this.img1 = img1;
        this.img2 = img2;
        this.img4 = img4;
        this.img3 = img3;
        this.img5 = img5;
        this.direccion = direccion;
        this.userfacebook = userfacebook;
        this.usertwitter = usertwitter;
    }

    public Customer() {
        setImages();
    }

    public void setImages() {
        images[0] = "img1";
        images[1] = "img2";
        images[2] = "img3";
        images[3] = "img4";
        images[4] = "img5";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getGoogleplus() {
        return googleplus;
    }

    public void setGoogleplus(String googleplus) {
        this.googleplus = googleplus;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getImgMenu() {
        return imgMenu;
    }

    public void setImgMenu(String imgMenu) {
        this.imgMenu = imgMenu;
    }

    public String getImgPortada() {
        return imgPortada;
    }

    public void setImgPortada(String imgPortada) {
        this.imgPortada = imgPortada;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) { this.img5 = img5; }

    public String getDireccion() { return direccion; }

    public void setDireccion(String direccion) { this.direccion = direccion; }

    public String getUserfacebook() { return userfacebook; }

    public void setUserfacebook(String userfacebook) { this.userfacebook = userfacebook; }

    public String getUsertwitter() { return usertwitter; }

    public void setUsertwitter(String usertwitter) { this.usertwitter = usertwitter; }

}
